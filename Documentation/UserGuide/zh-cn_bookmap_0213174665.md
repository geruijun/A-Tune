# A-Tune使用指南

-   [前言](前言.md)
-   [法律申明](法律申明.md)
-   [认识A-Tune](认识A-Tune.md)
    -   [简介](简介.md)
    -   [架构](架构.md)
    -   [支持特性与业务模型](支持特性与业务模型.md)

-   [安装与部署](安装与部署.md)
    -   [软硬件要求](软硬件要求.md)
    -   [环境准备](环境准备.md)
    -   [安装A-Tune](安装A-Tune.md)
        -   [安装模式介绍](安装模式介绍.md)
        -   [安装操作](安装操作.md)

    -   [部署A-Tune](部署A-Tune.md)
        -   [配置介绍](配置介绍.md)

    -   [启动A-Tune](启动A-Tune.md)

-   [使用方法](使用方法.md)
    -   [查询负载类型](查询负载类型.md)
        -   [list](list.md)

    -   [自定义负载类型](自定义负载类型.md)
        -   [define](define.md)
        -   [update](update.md)
        -   [undefine](undefine.md)

    -   [自定义模型](自定义模型.md)
        -   [collection](collection.md)
        -   [train](train.md)

    -   [分析负载类型并自优化](分析负载类型并自优化.md)
        -   [analysis](analysis.md)

    -   [查询profile](查询profile.md)
        -   [info](info.md)

    -   [设置profile](设置profile.md)
        -   [profile](profile.md)

    -   [回滚profile](回滚profile.md)
        -   [rollback](rollback.md)

    -   [更新数据库](更新数据库.md)
        -   [upgrade](upgrade.md)

    -   [系统信息查询](系统信息查询.md)
        -   [check](check.md)

    -   [参数自调优](参数自调优.md)
        -   [tuning](tuning.md)


-   [附录](附录.md)
    -   [术语和缩略语](术语和缩略语.md)


